function erro = erroRelativo(x1, x2)
%ERRORELATIVO C�lculo do erro relativo
%
%   Uso: erroRelativo(x1, x2)
%   
%   Entradas 
%       x1 -> valor aproximado
%       x2 -> valor exato
%
%   Saida
%       erro -> raz�o entre o erro e o valor exato

if x2==0
    erro = x1;
else
    erro = abs((x1-x2)./x1);

end