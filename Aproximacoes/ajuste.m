function [ beta yEst ] = ajuste( x, y, m )
%AJUSTE Summary of this function goes here
%   Detailed explanation goes here

    X = VMonde(x,m);
    A = X'*X;
    b = X'*y;
    [L U slgauss] = gauss2(A,b);
    beta = subReg(slgauss);
    yEst = X*beta;

end

