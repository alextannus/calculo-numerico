function [ pz ] = poliNewton( m, x, y, z )
%POLINEWTON Summary of this function goes here
%   Detailed explanation goes here

for i=1:m
    dely(i)=y(i);
end

for i=1:m-1
    for k=m:-1:i+1
        dely(k) = (dely(k)-dely(k-1))/(x(k)-x(k-i));
    end
end

pz = dely(m);
for i=(m-1):-1:1
    pz=pz*(z-x(i))+dely(i);
end

end

