function [ Pz ] = poliLagrange( m,x,y,z )
%POLILAGRANGE Summary of this function goes here
%   Detailed explanation goes here

%m = size(x,1);

Pz=0;
for i=1:m
    c=1;
    d=1;
    for j=1:m
        if i~=j
            c = c*(z-x(j));
            d = d*(x(i)-x(j));
        end
    end
    Pz = Pz + y(i)*c/d;
end

end

