function [ mv ] = VMonde( x, m )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

    tamX = size(x,1);
    mv = ones(tamX, m+1);
    
    for i=1:m
        mv(:,i)=x.^(m-i+1);
    end       

end

