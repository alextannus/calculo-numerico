clear all;
close all;
clc;

x = textread('Xi.txt');
y = textread('fXi.txt');

plot(x,y,'go');
hold;

ordem = 3;

ind = 1:ordem:59;

for i = 1:(size(ind,2)-1)
    ini = ind(i);
    fim = ind(i+1);
    yEst = ajuste(x(ini:fim),y(ini:fim),ordem);
    plot(x(ini:fim),yEst,'r');
end
