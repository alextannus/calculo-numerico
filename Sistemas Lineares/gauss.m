function [ resp ] = gauss( A, b )
%GAUSS Summary of this function goes here
%   Detailed explanation goes here
    n = size(A,1);
    X = [A b];
    resp = zeros(3,1);
   
    for i = 1:n-1
        for j = (i+1):n
            if (X(i,i) == 0)
                disp('Matriz singular');
            else
                div = X(j,i)/X(i,i);
                X(j,:) = X(j,:)-div*X(i,:);
            end
        end
    end
    
    resp(n)=X(n,n+1)/X(n,n);
    
    for i = (n-1):-1:1
        sum=0;
        for j = i+1:n
            sum = sum+X(i,j)*resp(j)
        end
        resp(i) = (X(i, n+1) - sum)/X(i,i);
    end
    
    U = X(:,1:n);
    

end

