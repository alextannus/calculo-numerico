function [ matriz ] = pivoParcial( matriz, linha )
%PIVOPARCIAL C�lculo de pivoteamento parcial
%
%   Uso: pivoParcial(matriz,linha)
%
%   matriz -> matriz aumentada. 
%   linha -> posicao do pivo na matriz
%
%   Descri��o: Esta fun��o pode ser utilizada em situa��es onde o
%   pivoteamento parcial seja necess�rio, tais como: elimina��o gaussiana e
%   m�todos iterativos (Gauss-Seidel, Gauss-Jacobi) de resolu��o de
%   sistemas lineares

    %C�lculo do n�mero de linhas da matriz
    n = size(matriz,1);
    
    %Obten��o do valor m�ximo e sua localiza��o na matriz
    maximo = max(abs(matriz(linha:n,linha)));
    loc = find(abs(matriz(:,linha)) == maximo);
    
    %Pivoteamento - troca de linhas
    tmp = matriz(linha,:);
    matriz(linha,:) = matriz(loc(1),:);
    matriz(loc(1),:) = tmp;

end

