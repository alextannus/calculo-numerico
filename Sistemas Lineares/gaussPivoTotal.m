function [ resposta ] = gaussPivoTotal( A, b )
%GAUSSPIVOTOTAL Summary of this function goes here
%   Detailed explanation goes here

    n = size(A,1);
    X = [A b];

    vet=[];
    
    for i = 1:n-1
        [X troca] = pivoTotal(X,i);
        div = X((i+1):n,i)/X(i,i);
        X((i+1):n,:) = X((i+1):n,:) - div*X(i,:);
        if numel(troca)
            vet = [vet; troca];
        end
    end
    
    
    resposta = subReg(X);

    
    
    for i=1:size(vet,1)
        if vet(i,1) ~= vet(i,3)
            tmp = resposta(vet(i,1));
            resposta(vet(i,1)) = resposta(vet(i,3));
            resposta(vet(i,3)) = tmp;
        end
    end
end

