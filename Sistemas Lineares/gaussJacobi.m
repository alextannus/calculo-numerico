function [ resposta ] = gaussJacobi( A, b, tol )
%GAUSSJACOBI Resolu��o de sistemas lineares pelo m�todo iterativo Gauss
%Jacobi
%   Uso: gaussJacobi(A,b, tol)
%   A -> Matriz de coeficientes
%   b -> vetor de vari�veis independentes
%   tol -> valor m�nimo do erro desejado
%   
%   Refer�ncia: PIRES, A.A. C�lculo Num�rico: Pr�tica como Algoritmos e
%   Planilhas, S�o Paulo, Atlas, 2015 pp 179-185
n = size(A,1);
x=zeros(n,1);

erro = inf;
iter =0;
C = A;
g = b;

%% C�lculo da matriz C e do vetor g -> x(k+1)=C*x(k)+g
for i=1:n
    C(i,:) = -A(i,:)/A(i,i); %C(m,n)=-A(m,n)/A(m,m)
    C(i,i) = 0;  % diagonal principal zerada
    g(i)=b(i)/A(i,i); 
end

%% Resolu��o do sistema linear
while (erro > tol)
    xAnt=x;
    for i=1:n
        x(i) = (C(i,:)*xAnt+g(i)); % C�lculo da resposta
    end
    erro = max(abs(x-xAnt))/max(abs(xAnt)); %C�lculo do erro
end

resposta=x;

end

