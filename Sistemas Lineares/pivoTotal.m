function [ matriz, troca] = pivoTotal( matriz, indice )
%UNTITLED2 C�lculo de pivoteamento parcial
%
%   Uso: pivoTotal(matriz,linha)
%
%   matriz -> matriz aumentada. 
%   linha -> posicao do pivo na matriz
%   troca -> vetor contendo informa��es das trocas realizadas
%   Descri��o: Esta fun��o pode ser utilizada em situa��es onde o
%   pivoteamento total seja necess�rio, tais como: elimina��o gaussiana e
%   m�todos iterativos (Gauss-Seidel, Gauss-Jacobi) de resolu��o de
%   sistemas lineares

    % C�lculo do n�mero de linhas da matriz
    n = size(matriz,1);
    
    % Obten��o do valor m�ximo e sua localiza��o na matriz
    maximo = max(max(abs(matriz(indice:n,1:n))));
    [x y] = find(abs(matriz(:,1:n)) == maximo);
    
    % Armazenamento do vetor de trocas
    troca = [indice x(1) y(1)];
    
    %Pivoteamento - troca de linhas
    tmp = matriz(indice,:);
    matriz(indice,:) = matriz(x(1),:);
    matriz(x(1),:) = tmp;
    
    %Pivoteamento - troca de colunas
    tmp = matriz(:,indice);
    matriz(:,indice) = matriz(:,y(1));
    matriz(:,y(1)) = tmp;

end

