function [ resposta ] = gaussPivoParcial( A, b )
%GAUSSPIVOPARCIAL Summary of this function goes here
%   Detailed explanation goes here

    n = size(A,1);
    X = [A b];

    for i = 1:n-1
        X = pivoParcial(X,i);
        div = X((i+1):n,i)/X(i,i);
        X((i+1):n,:) = X((i+1):n,:) - div*X(i,:);
        
    end

    resposta = subReg(X);  
    
end



