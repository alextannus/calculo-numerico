function [ invMat ] = inversa( A )
%INVERSA C�lculo da matriz inversa
%
%   Uso: inversa(A)
%   
%   Entrada
%       A -> matriz quadrada
%
%   Sa�da
%       invMat -> matriz inversa

    % Calcular as matrizes triangulares
    [L U] = fatoraLU(A);

    % Par�metros de controle
    n = size(L,1);
    id = eye(n); % Matriz identidade utilizada para obten��o de vetores unit�rios
    invMat = zeros(n,n); % Aloca��o de espa�o da matriz inversa

    for i=1:n
        % Resolver L*d=vet
        X = [L id(:,i)]
        d = subProg(X); 
        % Resolver U*x=d
        Z = [U d];
        x = substituicao(Z);
        % Preencher matriz inversa
        invMat(:,i) = x;
    end

end

% Autor: Alexandre Tannus
% Data: 13/09/18