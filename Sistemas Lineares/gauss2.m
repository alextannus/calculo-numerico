function [ L U X ] = gauss2( A, b )
%GAUSS2 Summary of this function goes here
%   Detailed explanation goes here

    n = size(A,1);
    X = [A b];
    
    
    
    L = eye(n);
    for i = 1:n-1
        if X(i,i) == 0
            disp('Matriz singular');
            break;
        else
            div = X((i+1):n,i)/X(i,i);
            L((i+1:n),i) = div;
            X((i+1):n,:) = X((i+1):n,:) - div*X(i,:);
        end
    end

    U = X(:,1:n);

    
    
end

