function [ det ] = determinante( A )
%DETERMINANTE Summary of this function goes here
%   Detailed explanation goes here

    [L, U] = fatoraLU(A,0);

    n = size(U,1);
    
    det = 1;

    for i = 1:n
        det = det * U(i,i);
    end

end

