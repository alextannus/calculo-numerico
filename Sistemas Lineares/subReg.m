function [ resp ] = subReg( matriz )
%SUBSTITUICAO Resolu��o de sistemas lineares por subtitui��o regressiva

%   matriz -> matriz de entrada de dados. Deve ser uma matriz aumentada de
%   tamanho n x (n+1)
%   resp -> resposta do sistema linear 

    if size(matriz,2) ~= size(matriz,1)+1 %teste de conformidade do tamanho
        disp('O tamanho da matriz de entrada est� errado. A matriz deve possuir n linhas e n+1 colunas');
    else
        
        n = size(matriz,1);
        resp = zeros(n,1);
        % Passo base - c�lculo do valor de x_n
        resp(n) = matriz(n,n+1)/matriz(n,n);

        % Substitui��o regressiva
        for i = (n-1):-1:1
            resp(i)= (matriz(i,n+1)-matriz(i,(i+1):n)*resp((i+1):n))/matriz(i,i);
        end
    end
end

