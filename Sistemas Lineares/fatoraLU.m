function [ L, U ] = fatoraLU( A, b )
%FATORALU Fatora��o LU
%
%   Descri��o: C�lculo da fatora��o LU de matrizes. As matrizes
%   triangulares obtidas podem ser utilizadas para a resolu��o de sistemas
%   lineares e para o c�lculo de determinante
%
%   Uso: fatoraLU(A,b)
%
%   Entradas: 
%       A -> matriz de coeficientes 
%       b -> termos independentes
%
%   Sa�das:
%       L -> matriz triangular inferior (zeros acima da diagonal)
%       U -> matriz triangular superior (zeros abaixo da diagonal)
    
    n = size(A,1);
    if b == 0
        b = zeros(n,1);
    end
    X = [A b];
    
    L = eye(n);
   
    for i = 1:n-1
        if X(i,i) == 0
            X=pivoParcial(X,i);
            break;
        end
        div = X((i+1):n,i)/X(i,i);
        L((i+1:n),i) = div;
        X((i+1):n,:) = X((i+1):n,:) - div*X(i,:);
        
    end
    
    U = X(:,1:n);
    
    
    
end

