function [ resp ] = subProg( matriz )
%SUBPROG Summary of this function goes here
%   Detailed explanation goes here

    n = size(matriz,1);
    resp = zeros(n,1);
    
    resp(1) = matriz(1,n+1)/matriz(1,1);

    for i = 2:n
        resp(i) = (matriz(i,n+1)-matriz(i,1:(i-1))*resp(1:(i-1)))/matriz(i,i);
    end

end

