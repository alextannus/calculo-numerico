function [ resp ] = newtonMod(funcao1, funcao2, x0, y0, tol, iterMax)
%NEWTONMOD Summary of this function goes here
%   Detailed explanation goes here
    vars = symvar(funcao1);
    x=vars(1);
    y=vars(2);
    f(x,y)=funcao1;
    g(x,y)=funcao2;
    fx(x,y) = diff(f,x);
    fy(x,y) = diff(f,y);
    gx(x,y) = diff(g,x);
    gy(x,y) = diff(g,y);
    jac(x,y)=fx(x,y)*gy(x,y)-gx(x,y)*fy(x,y);
    xEst = x0;
    yEst = y0;
    erro = [Inf Inf];
    iter=1;
    
    while max(erro) > tol && iter < iterMax
        xAtual = xEst;
        yAtual = yEst;
        iter=iter+1;
        
        xEst = double(xAtual - (f(xAtual,yAtual)*gy(xAtual,yAtual)-g(xAtual,yAtual)*fy(xAtual,yAtual))/jac(xAtual,yAtual));
        yEst = double(yAtual - (g(xEst,yAtual)*fx(xEst,yAtual)-f(xEst,yAtual)*gx(xEst,yAtual))/jac(xEst,yAtual));
        
        erro = erroRelativo([xAtual, yAtual], [xEst, yEst]);
    end
    
    resp=double([iter xEst, erro(1);iter yEst erro(2)])
    
end



