function [ resposta ] = newtonRaphson( vars, funcao, intervalo, tol, iterMax )
%NEWTONRAPHSON Summary of this function goes here
%   Detailed explanation goes here
    
    %syms x;
    assume(vars, 'real');
    iter = 1;
    f(vars) = funcao;
    df(vars) = diff(funcao);
    erro = Inf;
    xEst = mean(intervalo);

     resposta = vpa([iter xEst erro],7); %defini��o inicial da matriz de respostas
     
     while (iter < iterMax && erro > tol)
         iter = iter+1;
         u = xEst;
         xEst = xEst - f(xEst)/df(xEst);
         erro = erroRelativo(u, xEst);
         resposta = double([resposta; iter xEst erro]);        
     end
end

