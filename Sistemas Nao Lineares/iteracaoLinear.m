function [resp] = iteracaoLinear(funcao1, funcao2, x0, y0, tol, iterMax)
%NEWTON Summary of this function goes here
%   Detailed explanation goes here

    vars = symvar(funcao1);
    x=vars(1);
    y=vars(2);
    f1(vars)=funcao1;
    f2(vars)=funcao2;
    df1x(vars) = diff(f1,x);
    df1y(vars) = diff(f1,y);
    df2x(vars) = diff(f2,x);
    df2y(vars) = diff(f2,y);
    
    xEst = x0;
    yEst = y0;
    erro = [Inf Inf];
    iter=1;
    
    while max(erro) > tol && iter < iterMax
        xAtual = xEst;
        yAtual = yEst;
        if (df1x(xEst,yEst)+df1y(xEst,yEst)) < 1 && (df2x(xEst,yEst)+df2y(xEst,yEst)) < 1 
            xEst = f1(xAtual, yAtual);
            yEst = f2(xAtual, yAtual);
        end
        erro = erroRelativo([xAtual, yAtual], [xEst, yEst]);
        iter=iter+1;
    end
    
    resp=double([xEst, erro(1);yEst erro(2)])
    
    
    
    

end

