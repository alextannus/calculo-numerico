function [ resposta ] = falsaPosicao(vars, funcao, intervalo, tol, iterMax )
%FALSAPOSICAO C�lculo da raiz de equa��es atrav�s do m�todo da falsa
%posi��o
%
%   Uso: falsaPosicao(vars, funcao, intervalo, tol, iterMax)
%   
%   Entradas 
%       vars -> variaveis simb�licas utilizadas
%       funcao -> fun��o de interesse. Definida em rela��o � vari�vel
%       simb�lica
%       intervalo -> intervalo de interesse. Vetor com dois valores.
%       tol  -> menor varia��o aceit�vel. Condi��o de parada.
%       iter -> n�mero m�ximo de itera��es. Condi��o de parada
%
%   Saida
%       resposta -> matriz com os elementos [iteracao, xEst, erro], sendo xEst na �ltima linha o valor de x procurado desejada.


    assume(vars, 'real');  
    f(vars) = funcao;
    iter = 1; %valor inicial para itera��o
    xMin = intervalo(1);
    xMax = intervalo(2);
    
    % C�lculo de f(xMin) e f(xMax)
    fMin = f(xMin);
    fMax = f(xMax);

    xEst = double((xMin*fMax-xMax*fMin)/(fMax-fMin)); %estimativa inicial de x    

    
    fx = f(xEst);
    % Teste de converg�ncia. Para a fun��o convergir fMin e fMax devem ter
    % sinais opostos
    if (fMin > 0 && fMax > 0) || (fMin < 0 && fMax < 0)
        fprintf('Fun��o n�o convergir� para o intervalo [%.3f, %.3f].\nTente outro intervalo\n', xMin, xMax);
        return;
    end
    
    erro = Inf; % erro inicial
    
    
    resposta = [iter xMin xEst xMax erro]; %defini��o inicial da matriz de respostas
    
    
    while iter <iterMax && erro > tol
        if fMin*fx < 0
            xMax = xEst;
            fMax = fx;
        else
            xMin = xEst;
            fMin = fx;
        end
        iter = iter+1;
        u = xEst;
        xEst = double((xMin*fMax-xMax*fMin)/(fMax-fMin));
        fx = f(xEst);
        erro = erroRelativo(u, xEst);
        resposta = double([resposta; iter xMin xEst xMax erro]);
    end
    
end

