function [ resposta ] = secante( vars, funcao, intervalo, tol, iterMax  )
%SECANTE C�lculo da raiz de equa��es atrav�s do m�todo da secante
%
%   Uso: secante(vars, funcao, intervalo, tol, iterMax)
%   
%   Entradas 
%       vars -> variaveis simb�licas utilizadas
%       funcao -> fun��o de interesse. Definida em rela��o � vari�vel
%       simb�lica
%       intervalo -> intervalo de interesse. Vetor com dois valores.
%       tol  -> menor varia��o aceit�vel. Condi��o de parada.
%       iter -> n�mero m�ximo de itera��es. Condi��o de parada
%
%   Saida
%       resposta -> matriz com os elementos [iteracao, xEst, erro], sendo xEst na �ltima linha o valor de x procurado desejada.


    assume(vars, 'real');  
    f(vars) = funcao;
    iter = 1; %valor inicial para itera��o
    xAnt = intervalo(1);
    xAtual = intervalo(2);
    
    erro = Inf;
    
    resposta = [iter xAtual erro];
    
    while iter < iterMax && erro > tol
        fAnt = f(xAnt);
        fAtual = f(xAtual);
        xEst = double((xAnt*fAtual-xAtual*fAnt)/(fAtual-fAnt)); %estimativa inicial de x    
        erro = erroRelativo(xEst,xAtual);
        iter = iter + 1;               
        xAnt = xAtual;
        xAtual = xEst;
        resposta = [resposta; iter xAtual erro];
    end
    
end

