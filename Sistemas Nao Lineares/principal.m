% Script de resolu��o de equa��es n�o lineares
% M�todos utilizados:
    %Newton Raphson
    %Secante
    %Bisse��o
    %Falsa Posi��o

% Defini��o de vari�vel simb�lica
syms x;
% Defini��o da fun��o de interesse
% ATEN��O: A fun��o cos trabalha com valores em radianos. A fun��o que
% utiliza graus � cosd, mas ela n�o funciona com vari�veis simb�licas.
% Caso queira utilizar graus realize a convers�o usando a fun��o deg2rad
f = 1.67*cos(40)-2.5*cos(x)+1.83-cos(40-x);
g = 4*sin(x)-exp(x);

% Defini��o do intervalo de interesse - formato [a, b]
intervaloF = [pi, 2*pi];
intervaloG = [0, 0.5];

% Definicao de crit�rios de parada
tolerancia = 0.5*10^-3; %toler�ncia    
iteracoes = 50; %n�mero m�ximo de itera��es

%Chamada dos m�todos de resolu��o
rfp = falsaPosicao(x, f, intervaloF, tolerancia, iteracoes);
rb = bissecao(x, f, intervaloF, tolerancia, iteracoes)
rnr = newtonRaphson(x, g, intervaloG, tolerancia, iteracoes);
rs = secante(x, g, intervaloG, tolerancia, iteracoes);

% Impress�o dos resultados

fprintf('Bisse��o: %.8f\n', rb(end,3));
fprintf('Falsa Posi��o: %.8f\n', rfp(end,3));

fprintf('Newton Raphson: %.8f\n', rnr(end,2));
fprintf('Secante: %.8f\n', rs(end,2));

