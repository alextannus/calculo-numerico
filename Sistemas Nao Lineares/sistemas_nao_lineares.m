
clear all
close all
clc

syms x y;
vars = [x,y];


f = x+y^3-5*y^2-2*y-10;
g = x+y^3+y^2-14*y-29;

newton(f, g,15,-2,0.001,50)
newtonMod(f, g,15,-2,0.001,50)